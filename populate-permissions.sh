#!/usr/bin/env bash

while (( "${#}" )); do
    case "${1}" in
        -s|--server)
            SERVER=${2}
            shift 2
            ;;
        -d|--dicom-folder)
            DICOM_FOLDER=${2}
            shift 2
            ;;
        -P|--projects)
            PROJECT_COUNT=${2}
            shift 2
            ;;
        -S|--subjects)
            SUBJECT_COUNT=${2}
            shift 2
            ;;
        -H|--help)
            echo "populate-permissions.sh script"
            echo
            echo "Options:"
            echo
            echo " -s|--server <server>          Specifies the address of the XNAT server. Defaults to https://xnatdev.xnat.org."
            echo " -P|--projects <#>             Specifies the number of projects to create. Defaults to 2."
            echo " -S|--subjects <#>             Specifies the number of subjects to create per project. Defaults to 2."
            echo " -d|--dicom-folder <folder>    Specifies the folder containing a sample DICOM session to upload."
            echo " -H|--help"
            exit 0
            ;;
        *)
            echo "Error: unknown option ${1}" >&2
            exit 1
            ;;
    esac
done

OWD="$(dirname "${0}")"

[[ -z ${DICOM_FOLDER} ]] && { DICOM_FOLDER="${OWD}/dicom"; }
[[ -z ${SERVER} ]] && { SERVER="https://xnatdev.xnat.org"; }
[[ -z ${PROJECT_COUNT} ]] && { PROJECT_COUNT=2; }
[[ -z ${SUBJECT_COUNT} ]] && { SUBJECT_COUNT=2; }

source ${OWD}/macros.sh

LOG_FOLDER=${OWD}/logs/$(timestamp)
mkdir -p ${LOG_FOLDER}

initialize

addUser dataAdmin ALL_DATA_ADMIN
addUser dataAccess ALL_DATA_ACCESS

addProject admin Public public
addSubject admin Public Public_01
sendMrSession admin Public Public_01 Public_01_MR_01
addImageQc admin Public Public_01 Public_01_MR_01 Public_01_MR_01_QC

addProject admin Protected protected
addSubject admin Protected Protected_01
sendMrSession admin Protected Protected_01 Protected_01_MR_01
addImageQc admin Protected Protected_01 Protected_01_MR_01 Protected_01_MR_01_QC

for PROJECT_INDEX in $(seq -f "%02g" 1 ${PROJECT_COUNT}); do
    PROJECT_ID="XNAT_${PROJECT_INDEX}"
    OWNER_ID="owner${PROJECT_INDEX}"
    MEMBER_ID="member${PROJECT_INDEX}"
    COLLAB_ID="collab${PROJECT_INDEX}"
    addUser ${OWNER_ID}
    addUser ${MEMBER_ID}
    addUser ${COLLAB_ID}
    
    authUser ${OWNER_ID}
    
    addProject ${OWNER_ID} ${PROJECT_ID}
    addProjectUser ${OWNER_ID} ${PROJECT_ID} ${MEMBER_ID} member
    addProjectUser ${OWNER_ID} ${PROJECT_ID} ${COLLAB_ID} collaborator
    for SUBJECT_INDEX in $(seq -f "%02g" 1 ${SUBJECT_COUNT}); do
        SUBJECT_ID="${PROJECT_ID}_${SUBJECT_INDEX}"
        MR_SESSION_ID="${SUBJECT_ID}_MR_01"
        PET_SESSION_ID="${SUBJECT_ID}_FDG_01"
        addSubject ${OWNER_ID} ${PROJECT_ID} ${SUBJECT_ID}
        sendMrSession ${OWNER_ID} ${PROJECT_ID} ${SUBJECT_ID} ${MR_SESSION_ID}
        addImageQc ${OWNER_ID} ${PROJECT_ID} ${SUBJECT_ID} ${MR_SESSION_ID} ${MR_SESSION_ID}_QC
        sendPetSession ${OWNER_ID} ${PROJECT_ID} ${SUBJECT_ID} ${PET_SESSION_ID}
        addImageQc ${OWNER_ID} ${PROJECT_ID} ${SUBJECT_ID} ${PET_SESSION_ID} ${PET_SESSION_ID}_QC
    done
done

# Set up sharing manually:
#
# shareSubjectExperimentAssessor <user> <source_project> <source_subject> <source_session> <source_assessor> <target_project> <target_subject> <target_session> <target_assessor>
#
# To share XNAT_01_01_MR_01_QC and all of its ancestors to XNAT_02_01_MR_01_QC, you'd do:
#
# shareSubjectExperimentAssessor admin XNAT_01 XNAT_01_01 XNAT_01_01_MR_01 XNAT_01_01_MR_01_QC XNAT_02 XNAT_02_01 XNAT_02_01_MR_01 XNAT_02_01_MR_01_QC

shareSubjectExperimentAssessor admin XNAT_01 XNAT_01_01 XNAT_01_01_MR_01 XNAT_01_01_MR_01_QC Public Public_02 Public_02_MR_01 Public_02_MR_01_QC
shareSubjectExperimentAssessor admin XNAT_02 XNAT_02_01 XNAT_02_01_MR_01 XNAT_02_01_MR_01_QC Public Public_03 Public_03_MR_01 Public_03_MR_01_QC
