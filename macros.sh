#!/usr/bin/env bash

HTTP_OPTS="--verify=no --timeout=1800 --pretty=format --check-status --verbose"
HTTP_OPTS_BODY="--verify=no --timeout=1800 --pretty=format --body"

dateCmd() {
    gdate &> /dev/null
    [[ ${?} == 0 ]] && echo "gdate" || echo "date"
}

DATE_CMD=$(dateCmd)

uppercase() {
    echo "$(echo ${1:0:1} | tr '[:lower:]' '[:upper:]')${1:1}"
}

trim() {
    echo "$(echo ${1} | tr -d ' ')"
}

timestamp() {
  echo "$(${DATE_CMD} +%Y%m%d%H%M%S%N | cut -b1-17)"
}

# Send session function
sendMrSession() {
    sendImageSession MR "${@}"
}

sendPetSession() {
    sendImageSession PET "${@}"
}

sendImageSession() {
    local MODALITY=${1}
    local SENDER=${2}
    local PROJECT=${3}
    local SUBJECT=${4}
    local SESSION=${5}

    echo "Uploading ${MODALITY} session ${SESSION} to ${PROJECT}/${SUBJECT} as ${SENDER}"
    for DIR in $(find ${DICOM_FOLDER}/${MODALITY} -mindepth 1 -maxdepth 1 -type d); do
        echo " * Sending files from folder ${DIR}"
        zip -q - $(find ${DIR} -type f -name "*.dcm") | http --session=${SENDER} ${HTTP_OPTS_BODY} POST ${SERVER}/data/services/import import-handler==DICOM-zip PROJECT_ID==${PROJECT} SUBJECT_ID==${SUBJECT} EXPT_LABEL==${SESSION} source==script rename==true overwrite==true inbody==true > .uri
        [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS_BODY} POST ${SERVER}/data/services/import import-handler==DICOM-zip PROJECT_ID==${PROJECT} SUBJECT_ID==${SUBJECT} EXPT_LABEL==${SESSION} source==script rename==true overwrite==true inbody==true"; exit 255; }
    done

    URI=$(fgrep /prearchive/projects .uri | sed 's#[^A-Za-z0-9_/-]##g')
    [[ -z ${URI} ]] && { echo "An error of some sort occurred when uploading the session ${SESSION} to project/subject ${PROJECT}/${SUBJECT}: I don't have a valid URI in response."; exit 255; }
    rm -f .uri
    echo "Committing session at ${URI}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log POST ${SERVER}${URI} action==commit source==script
}

addImageQc() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local SESSION=${4}
    local ASSESSOR="${5}"
    local SESSION_ID=$(http --session=${SENDER} ${HTTP_OPTS_BODY} ${SERVER}/data/projects/${PROJECT}/experiments/${SESSION} format==json | jq ".items[0].data_fields.ID" | tr -d '"')

    echo "Uploading image QC ${ASSESSOR} for ${SESSION} in ${PROJECT}/${SUBJECT} as ${SENDER}"

    cat image_qc.xml | sed 's/@PROJECT@/'"${PROJECT}"'/' | sed 's/@SESSION_ID@/'"${SESSION_ID}"'/' | sed 's/@LABEL@/'"${ASSESSOR}"'/' | sed 's/@DATE@/'$(${DATE_CMD} -I)'/' | http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log POST ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/assessors inbody==true
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS} POST ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/assessors inbody==true"; exit 255; }
}

addProject() {
    local SENDER=${1}
    local PROJECT=${2}
    local LABEL=${PROJECT//_/ }
    [[ -z ${3} ]] && { local ACCESS="private"; } || { local ACCESS=${3}; }

    echo "Creating project ${PROJECT} with label ${LABEL} as ${SENDER}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT} secondary_ID=="${LABEL}" name=="${LABEL}" accessibility==${ACCESS}
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT} secondary_ID=="${LABEL}" name=="${LABEL}" accessibility==${ACCESS}"; exit 255; }
}

addSubject() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}

    echo "Creating subject ${SUBJECT} in project ${PROJECT} as ${SENDER}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}"; exit 255; }
}

addProjectUser() {
    local USERNAME=${1}
    local PROJECT=${2}
    local MEMBER=${3}
    local LEVEL=${4}

    echo "Adding user ${MEMBER} to project ${PROJECT} at ${LEVEL} as ${USERNAME}"
    http --session=${USERNAME} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/users/${LEVEL}/${MEMBER}
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${USERNAME} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/users/${LEVEL}/${MEMBER}"; exit 255; }
}

addUser() {
    local USERNAME=${1}
    local GROUP=${2}
    echo "Adding user ${USERNAME}"
    http --session=admin ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log POST ${SERVER}/xapi/users firstName="$(uppercase ${USERNAME})" lastName="Jones" username="${USERNAME}" password="${USERNAME}" email="xnatselenium@gmail.com" enabled=true verified=true
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=admin ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log POST ${SERVER}/xapi/users firstName=\"$(uppercase ${USERNAME})\" lastName=\"Jones\" username=\"${USERNAME}\" password=\"${USERNAME}\" email=\"xnatselenium@gmail.com\" enabled=true verified=true"; exit 255; }
    [[ ! -z ${GROUP} ]] && { addGroupUser ${USERNAME} ${GROUP}; }
}

addGroupUser() {
    local USERNAME=${1}
    local GROUP=${2}
    echo "Adding user ${USERNAME} to group ${GROUP}"
    http --session=admin ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/xapi/users/${USERNAME}/groups/${GROUP}
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=admin ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/xapi/users/${USERNAME}/groups/${GROUP}"; exit 255; }
}

authUser() {
    local USERNAME=${1}
    echo "Authenticating user ${USERNAME}"
    http --auth=${USERNAME}:${USERNAME} --session=${USERNAME} --output=${LOG_FOLDER}/$(timestamp).log ${HTTP_OPTS} ${SERVER}/data/JSESSION
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --auth=${1}:${1} --session=${1} --output=${LOG_FOLDER}/$(timestamp).log ${HTTP_OPTS} ${SERVER}/data/JSESSION"; exit 255; }
}

# shareSubject <user> <source_project> <source_subject> <shared_project> [<shared_subject>]
# If <shared_subject> isn't specified, uses the same label for the shared subject
shareSubject() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local TARGET_PROJECT=${4}
    [[ -z ${5} ]] && { local TARGET_SUBJECT=${SUBJECT}; } || { local TARGET_SUBJECT=${5}; }

    echo "Sharing subject ${SUBJECT} from project ${PROJECT} to project ${TARGET_PROJECT} as user ${TARGET_SUBJECT}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/projects/${TARGET_PROJECT} event_reason=="standard sharing" label=="${TARGET_SUBJECT}"
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/projects/${TARGET_PROJECT} event_reason==\"standard sharing\" label==\"${TARGET_SUBJECT}\""; exit 255; }
}

# shareExperiment <user> <source_project> <source_subject> <source_experiment> <shared_project> [<shared_experiment>]
# If <shared_experiment> isn't specified, uses the same label for the shared experiment
shareExperiment() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local EXPERIMENT=${4}
    local TARGET_PROJECT=${5}
    [[ -z ${6} ]] && { local TARGET_EXPERIMENT=${EXPERIMENT}; } || { local TARGET_EXPERIMENT=${6}; }

    echo "Sharing experiment ${EXPERIMENT} from project ${PROJECT} to project ${TARGET_PROJECT} as experiment ${TARGET_EXPERIMENT}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT}/projects/${TARGET_PROJECT} event_reason=="standard sharing" label=="${TARGET_EXPERIMENT}"
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=${SENDER} ${HTTP_OPTS} PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT}/projects/${TARGET_PROJECT} event_reason==\"standard sharing\" label==\"${TARGET_EXPERIMENT}\""; exit 255; }
}

# shareAssessor <user> <source_project> <source_subject> <source_experiment> <source_assessor> <shared_project> [<shared_assessor>]
# If <shared_assessor> isn't specified, uses the same label for the shared assessor
shareAssessor() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local EXPERIMENT=${4}
    local ASSESSOR=${5}
    local TARGET_PROJECT=${6}
    [[ -z ${7} ]] && { local TARGET_ASSESSOR=${ASSESSOR}; } || { local TARGET_ASSESSOR=${7}; }

    echo "Sharing assessor ${ASSESSOR} from project ${PROJECT} to project ${TARGET_PROJECT} as experiment ${TARGET_ASSESSOR}"
    http --session=${SENDER} ${HTTP_OPTS} --output=${LOG_FOLDER}/$(timestamp).log PUT ${SERVER}/data/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${EXPERIMENT}/assessors/${ASSESSOR}/projects/${TARGET_PROJECT} event_reason=="standard sharing" label=="${TARGET_ASSESSOR}"
    [[ "${?}" -gt 3 ]] && { echo "An operation failed: "; exit 255; }
}

# shareSubjectExperiment <user> <source_project> <source_subject> <source_experiment> <shared_project> [<shared_subject> [<shared_experiment>]]
# If <shared_subject> or <shared_experiment> aren't specified, uses the same label for the shared subject and experiment
shareSubjectExperiment() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local EXPERIMENT=${4}
    local TARGET_PROJECT=${5}
    [[ -z ${6} ]] && { local TARGET_SUBJECT=${SUBJECT}; } || { local TARGET_SUBJECT=${6}; }
    [[ -z ${7} ]] && { local TARGET_EXPERIMENT=${EXPERIMENT}; } || { local TARGET_EXPERIMENT=${7}; }
    shareSubject ${SENDER} ${PROJECT} ${SUBJECT} ${TARGET_PROJECT} ${TARGET_SUBJECT}
    shareExperiment ${SENDER} ${PROJECT} ${SUBJECT} ${EXPERIMENT} ${TARGET_PROJECT} ${TARGET_SUBJECT}
}

# shareSubjectExperiment <user> <source_project> <source_subject> <source_experiment> <shared_project> [<shared_subject> [<shared_experiment> [<shared_assessor]]]
# If <shared_subject>, <shared_experiment>, or <shared_assessor> aren't specified, uses the same label for the shared subject, experiment, and assessor
shareSubjectExperimentAssessor() {
    local SENDER=${1}
    local PROJECT=${2}
    local SUBJECT=${3}
    local EXPERIMENT=${4}
    local ASSESSOR=${5}
    local TARGET_PROJECT=${6}
    [[ -z ${7} ]] && { local TARGET_SUBJECT=${SUBJECT}; } || { local TARGET_SUBJECT=${7}; }
    [[ -z ${8} ]] && { local TARGET_EXPERIMENT=${EXPERIMENT}; } || { local TARGET_EXPERIMENT=${8}; }
    [[ -z ${9} ]] && { local TARGET_ASSESSOR=${EXPERIMENT}; } || { local TARGET_ASSESSOR=${9}; }
    shareSubjectExperiment ${SENDER} ${PROJECT} ${SUBJECT} ${EXPERIMENT} ${TARGET_PROJECT} ${TARGET_SUBJECT} ${TARGET_EXPERIMENT}
    shareAssessor ${SENDER} ${PROJECT} ${SUBJECT} ${EXPERIMENT} ${ASSESSOR} ${TARGET_PROJECT} ${TARGET_ASSESSOR}
}

initialize() {
    authUser admin

    local INITIALIZED=$(http --session=admin --body ${HTTP_OPTS} https://xnatdev.xnat.org/xapi/siteConfig/initialized | grep -E '(true|false)')
    [[ ${INITIALIZED} = "false" ]] && {
        echo "System not initialized, setting to default configuration"
        http --session=admin --verify=no --timeout=1800 --pretty=format --output=${LOG_FOLDER}/$(timestamp).log POST https://xnatdev.xnat.org/xapi/siteConfig adminEmail="xnatselenium@gmail.com" siteUrl=https://xnatdev.xnat.org initialized=true siteDescriptionText="Hi there, how are you doing?" emailVerification=true userRegistration=true
        [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=admin --verify=no --timeout=1800 --pretty=format POST https://xnatdev.xnat.org/xapi/siteConfig adminEmail=\"xnatselenium@gmail.com\" siteUrl=https://xnatdev.xnat.org initialized=true siteDescriptionText=\"Hi there, how are you doing?\" emailVerification=true userRegistration=true"; exit 255; }
        http --session=admin --verify=no --timeout=1800 --pretty=format --output=${LOG_FOLDER}/$(timestamp).log POST https://xnatdev.xnat.org/xapi/notifications smtpEnabled=true smtpHostname=localhost
        [[ "${?}" -gt 3 ]] && { echo "An operation failed: http --session=admin --verify=no --timeout=1800 --pretty=format POST https://xnatdev.xnat.org/xapi/notifications smtpEnabled=true smtpHostname=localhost"; exit 255; }
    }
}
